from typing import Optional
from pathlib import Path
from dataclasses import dataclass
from enum import Enum


@dataclass
class FoundAsset:
	loc_str: str
	path: Optional[Path]
	exists: Optional[bool]
	document_path: Path
	document_root: Optional[Path]
	is_local: bool
	in_doc_root: Optional[bool]

	@staticmethod
	def check_local(loc_str: str) -> bool:
		if "://" in loc_str:
			return False
		if loc_str.startswith("mailto:"):
			return False
		return True

	@classmethod
	def from_tag(cls, loc_str: str, doc_path: Path, doc_root: Optional[Path] = None):
		is_local = cls.check_local(loc_str)
		in_doc_root = None
		exists = None
		if is_local:
			path = (doc_path.parent / loc_str).resolve()
			exists = path.is_file()
			if doc_root is not None:
				in_doc_root = path.is_relative_to(doc_root)
		else:
			path = None
		return cls(
			loc_str=loc_str,
			path=path,
			exists=exists,
			document_path=doc_path,
			document_root=doc_root,
			is_local=is_local,
			in_doc_root=in_doc_root
		)
