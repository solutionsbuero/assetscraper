from typing import Union, Optional, List
from pathlib import Path
import yaml
from bs4 import BeautifulSoup

from assetscraper import FoundAsset


class Scraper:

	@staticmethod
	def _get_dir_path(p: Optional[Union[Path, str]]) -> Optional[Path]:
		if p is None:
			return p
		p = Path(p)
		if p.is_file():
			return p.parent

	@staticmethod
	def _get_tags():
		p = Path(__file__).parent / "tags.yaml"
		with p.open('r') as f:
			data = yaml.safe_load(f)
		return data

	def __init__(
		self,
		doc_root: Optional[Union[Path, str]] = None,
		get_remote_assets: bool = False,
		recurse_local_html: bool = False
	):
		self.doc_root: Optional[Path] = None if doc_root is None else Path(doc_root)
		self.get_remote_assets: bool = get_remote_assets
		self.recurse_local_html: bool = recurse_local_html

	def _collect(self, document: Path) -> List[FoundAsset]:
		assets = []
		with document.open('r') as f:
			content = f.read()
		soup = BeautifulSoup(content, "html.parser")
		taglist = Scraper._get_tags()
		for k, v in taglist['assets'].items():
			for i in v:
				tags = soup.find_all(i)
				for j in tags:
					loc_str = j.get(k, None)
					if loc_str is not None:
						assets.append(FoundAsset.from_tag(loc_str, document, self.doc_root))
		return assets


if __name__ == "__main__":
	sc = Scraper(doc_root=Path("/home/dinu/git/solutionsbuero/sb/projects/matthias-liechti/uhrwerk"))
	data = sc._collect(Path("/home/dinu/git/solutionsbuero/sb/projects/matthias-liechti/uhrwerk/032cefc0-0510-4b8f-874a-125749b5199dposttmpl_.html"))
	print(data)
